package org.example;

import org.example.controller.CalculatorController;

public class Main {
    public static void main(String[] args) {
        CalculatorController calculatorController = new CalculatorController();
        calculatorController.run();
    }
}
