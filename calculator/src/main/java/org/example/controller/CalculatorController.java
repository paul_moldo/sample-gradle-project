package org.example.controller;

import org.example.service.CalculatorService;

import java.util.Objects;
import java.util.Scanner;

public class CalculatorController {
    private final CalculatorService calculatorService;

    public CalculatorController(){
        calculatorService = new CalculatorService();
    }

    public void run() {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Input first number: ");
        double a = scanner.nextDouble();

        while(true) {
            System.out.print("Enter an operation (+, -, *, /, min, max, sqrt, exit): ");
            String operator = scanner.next();

            if(Objects.equals(operator, "exit"))
                break;

            if (!operator.equals("sqrt")) {
                System.out.print("Input next number: ");
            }
            double b = operator.equals("sqrt") ? -1 : scanner.nextDouble();

            try {
                double result = calculatorService.calculator(operator, a, b);
                System.out.println("Result: " + result);
                a = result;
            } catch (IllegalArgumentException e) {
                System.out.println("Error: " + e.getMessage());
            }
        }
    }
}
