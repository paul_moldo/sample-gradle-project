package org.example.domain;

public class Addition implements IOperation {
    @Override
    public double calculator(double a, double b) {
        return a + b;
    }
}