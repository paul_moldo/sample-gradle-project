package org.example.domain;

public class Division implements IOperation {
    @Override
    public double calculator(double a, double b) {
        return a / b;
    }
}
