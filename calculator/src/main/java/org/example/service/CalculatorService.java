package org.example.service;

import org.example.domain.Addition;
import org.example.domain.Division;
import org.example.domain.Subtraction;
import org.example.domain.Multiplication;
import org.example.domain.Minimum;
import org.example.domain.Maximum;
import org.example.domain.SquareRoot;
import org.example.domain.IOperation;
import java.util.Map;
import java.util.HashMap;

public class CalculatorService {
    private final Map<String, IOperation> operations;

    public CalculatorService(){
        operations = new HashMap<>();
        operations.put("+", new Addition());
        operations.put("-", new Subtraction());
        operations.put("*", new Multiplication());
        operations.put("/", new Division());
        operations.put("min", new Minimum());
        operations.put("max", new Maximum());
        operations.put("sqrt", new SquareRoot());
    }

    public double calculator(String operator, double a, double b){
        IOperation operation = operations.get(operator);
        if (operation == null){
            throw new IllegalArgumentException("Invalid given operation!");
        }
        return operation.calculator(a, b);
    }
}
